import React, {Component} from 'react';
import {Route,Switch,Redirect} from 'react-router-dom';
import Login from "./components/login";
import Main from "./components/main";
import { unstable_createMuiStrictModeTheme } from '@material-ui/core/styles';
//import Theme from './theme/theme'
import theme from './theme/styled_theme'
import NotFund from "./components/not-found";
//import {ThemeProvider} from '@material-ui/core'
import {ThemeProvider} from "styled-components";

class App extends Component{
    render() {
        return(
            <div className="rr">
                <ThemeProvider theme={theme}>
                <div className="content">

                    <Switch>
                        <Route path="/login" component={Login}/>
                        <Route path="/not-found" component={NotFund}/>
                        <Route path="/" exact component={Main}/>
                        <Redirect to='/not-found'/>
                    </Switch>
                </div>
                </ThemeProvider>
            </div>
        )
    }
}
export default App;
