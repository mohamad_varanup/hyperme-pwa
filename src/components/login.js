import React from "react";
import {TextField} from "@material-ui/core";
import styled from 'styled-components'
import './login.css';
import {TextFiled} from '@material-ui/core';
import back from './back.png'
const Login = () => {


    const FormList = styled.div`
    background : #F5F5F5;
    text-align: center;
 
    `;
    const TextFieldOne = styled(TextField)`
   background: white;
    direction: rtl;
   width: 70%;
    `;
    const Head = styled.h1`
    color: tomato;
    font-family: IranSansWeb;
    `;

    const Image=styled.img`
    align-content: start;
    width: 80%;
    position: relative;
    margin: 0;
    padding: 0;
    left: 10%;
    `;

    const Title=styled.h1`
    font-family: IRANSansWeb;
    color: white;
    display: inline-block;
    width: 100%;
    font-size: 2rem;
    `;
    const Detail=styled.h1`
    font-family: IRANSansWeb;
    color: white;
    font-size: 1.2rem;
    display: inline-block;
    `;
    const Div=styled.div`
     position: absolute;
     left: 37%;
     top: 40px;
     text-align: right;
     display: inline-block;
    z-index: 1;
    align-items: start;
   `;
    return (
        <FormList className="Login">
            <div>
                <Image  src={back} alt="login-back"/>
                <Div>
                    <Title>ثبت نام</Title>
                    <Detail>برای ورود به اَپلیکیشن باید ثبت نام کنید</Detail>
                </Div>
            </div>
            <form noValidate autoComplete="off">

                <TextFieldOne label="کد ملی شماره موبایل" variant="outlined"/>
            </form>
        </FormList>

    )
};
export default Login;
